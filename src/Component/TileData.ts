

  import image1 from '../Images/image1.jpg';
  import image2 from '../Images/image2.jpg';
  import image3 from '../Images/image3.jpg';
  import image4 from '../Images/image4.png';
  import image5 from '../Images/image5.jpg';
  import image6 from '../Images/image6.jpg';
  import image7 from '../Images/image7.jpg';

  const tileData = [
   {
     img: image1,
      title: 'Image',
      author: 'author',
    },
    {
      img: image2,
       title: 'Image',
       author: 'author',
     },
     {
      img: image3,
       title: 'Image',
       author: 'author',
     },
     {
      img: image4,
       title: 'Image',
       author: 'author',
     },
    {
      img: image5,
       title: 'Image',
       author: 'author',
     },
     {
      img: image6,
       title: 'Image',
       author: 'author',
     },
     {
      img: image7,
       title: 'Image',
       author: 'author',
     },
   
  ];

  export default tileData;

  