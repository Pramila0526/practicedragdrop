import React,{Component} from 'react';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import tileData from './TileData';
import tileData1 from './TileData1';
import '../CSS/Widget.css';


export default class TitlebarGridList extends Component {
 render(){
  return (
    <div className='root'>
      <div className='vayNetwork1'>
    <GridList cellHeight={180} className='gridList'>
      <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
        <h1>Vayana Network1</h1>
      </GridListTile>
      {tileData.map((tile) => (
        <GridListTile key={tile.img}>
          <img src={tile.img} alt={tile.title} />
          <GridListTileBar
            title={tile.title}
            subtitle={<span>by: {tile.author}</span>}
            actionIcon={
              <IconButton aria-label={`info about ${tile.title}`} className='icon'>
                <InfoIcon />
              </IconButton>
            }
          />
        </GridListTile>
      ))}
    </GridList>
    </div>

<div>
<GridList cellHeight={180} className='gridList'>
      <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
        <h1>Vayana Network 2</h1>
      </GridListTile>
      {tileData1.map((tile) => (
        <GridListTile key={tile.img}>
          <img src={tile.img} alt={tile.title} />
          <GridListTileBar
            title={tile.title}
            subtitle={<span>by: {tile.author}</span>}
            actionIcon={
              <IconButton aria-label={`info about ${tile.title}`} className='icon'>
                <InfoIcon />
              </IconButton>
            }
          />
        </GridListTile>
      ))}
    </GridList>
    </div>
  </div>
         
  );
}
}
