import React from 'react';
import logo from './logo.svg';
import './App.css';
import Widgetclass from './Component/WidgetClassComponent';
import Widget from './Component/Widget'
import DragDrop from './DragDrop/DragDrop';
import DragAndDrop from './DragDrop/DragAndDrop'


function App() {
  return (
    // <Widget></Widget>
      // <Widgetclass></Widgetclass>
      // <DragDrop/>
      <DragAndDrop/>
  );
}

export default App;
